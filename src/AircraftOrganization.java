import Organization.*;

import java.util.List;

public class AircraftOrganization extends Organization {
    public AircraftOrganization(String name, Type type) {
        super(name, type);
    }



    @Override
    public double getNextFinancialReport() {
        int averagePlaneRequests = 5;
        double averageCostsPerPlane = 5461345.91;
        double averageSalaryPerWorker = 426985386.58;
        double averagePlanePrice = 411698789;
        int workers = 32;
        return (Math.random() + 0.5) * ((averagePlanePrice - averageCostsPerPlane) * averagePlaneRequests - averageSalaryPerWorker * workers);
    }
}
