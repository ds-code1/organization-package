import Organization.*;

public class InsuranceOrganization extends Organization {
    public InsuranceOrganization(String name, Type type) {
        super(name, type);
    }

    @Override
    public double getNextFinancialReport() {
        double insuranceAvailability = 0.2;
        return Math.random() + Math.random() + Math.random() * 53156 + insuranceAvailability;
    }
}
