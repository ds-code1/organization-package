
import Organization.Organization;
import Organization.OrganizationRegistry;
import Organization.Type;
import TUI.Menu;
import TUI.PrintUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
@SuppressWarnings("SpellCheckingInspection")
public class Main {

    public static List<String> generationList = List.of(
            "Перспектива",
            "Визави",
            "Сквозная",
            "Констракшн и так далее",
            "Разрывная",
            "Передовые дела",
            "Употребительная за углом",
            "Пилотируемый космос",
            "Упокойная душа",
            "Хорошо и ещё хуже",
            "Расточительная компания","Вкусно и тупо",
            "Напитки пригорья",
            "Зажиточный минимум"
    );

    public static List<String> kindsList = List.of("Авиазавод", "Судостроительная компания", "Страховая компания");

    public static PrintUtils printUtils = new PrintUtils();
    public static OrganizationRegistry orgRegistry = new OrganizationRegistry(printUtils);

    public static Menu mainMenu = new Menu(printUtils, List.of(
        new Menu.Option("Вывод списка организаций", () -> orgRegistry.printList()),
        new Menu.Option("Поиск организации по имени", ()-> orgRegistry.printList(
            orgRegistry.findMatchingByPartOfName(
                Menu.getUserInput("Введите имя"))
        )),
        new Menu.Option("Вывод информации об организации", ()->{
            try{
                Organization org = orgRegistry.getById(
                        Integer.parseInt(Menu.getUserInput("Введите id"))
                );
                if(org == null){
                    System.out.println("Организации с таким id не зарегистрирована");
                }
                else{
                    System.out.println(org);
                }
            } catch (NumberFormatException e){
                System.out.println("Id введено неверно");
            }
        }),
        new Menu.Option("Добавить организацию", ()->{
            String name = Menu.getUserInput("Введите имя");

            List<String> typeNames = Arrays.stream(Type.values()).map(item -> item.getFullName()).toList();
            List<Type> types = Arrays.stream(Type.values()).toList();
            printUtils.printColumn(typeNames, 80);
            Type type = Menu.getUserSelection("Выберите тип", types);

            printUtils.printColumn(kindsList, 80);
            String kind = Menu.getUserSelection("Выберите категорию", kindsList);

            boolean result = true;
            switch (kind){
                case ("Судостроительная компания") ->  result = orgRegistry.add(new ShipBuildingOrganization(name, type));
                case ("Страховая компания") ->  result = orgRegistry.add(new InsuranceOrganization(name, type));
                case ("Авиазавод") ->  result = orgRegistry.add(new AircraftOrganization(name, type));
            }

            if(!result){
                System.out.println("Такая организация уже существует");
            }else{
                System.out.println("Организация успешно зарегистрирована");
            }
        }),
        new Menu.Option("Удалить организацию", ()->{
            try{
                if(!orgRegistry.delete(
                        Integer.parseInt(Menu.getUserInput("Введите id"))
                )){
                    System.out.println("Организации с таким id не зарегистрирована");
                }
                else{
                    System.out.println("Организация удалена");
                }
            } catch (NumberFormatException e){
                System.out.println("Id введено неверно");
            }
        }),
        new Menu.Option("Получить прибыль за месяц", ()->{
            try{
                Organization org = orgRegistry.getById(
                        Integer.parseInt(Menu.getUserInput("Введите id"))
                );
                if(org == null){
                    System.out.println("Организации с таким id не зарегистрирована");
                }
                else{
                    System.out.println("Прибыль в этот раз составила: " + org.getNextFinancialReport());
                }
            } catch (NumberFormatException e){
                System.out.println("Id введено неверно");
            }
        })
    ));

    static {
        // generate random organizations but names is fixed anyway
        for (String name : generationList) {
            Type randType = Type.values()[new Random().nextInt(Type.values().length)];
            String randClass = kindsList.get(new Random().nextInt(2));
            if("Страховая компания".equals(randClass)){
                orgRegistry.add(new AircraftOrganization(name, randType));
            }
            else if("Авиазавод".equals(randClass)){
                orgRegistry.add(new InsuranceOrganization(name, randType));
            }
            else if("Строительная компания".equals(randClass)){
                orgRegistry.add(new ShipBuildingOrganization(name, randType));
            }
        }
    }

    public static void main(String[] args) {
        mainMenu.run();
    }

}