package Organization;

import java.util.List;
@SuppressWarnings("SpellCheckingInspection")
public abstract class Organization {

    public Organization(String name, Type type){
        this.name = name;
        this.type = type;
    }
    private int id = 0;
    private final String name;
    private final Type type;

    public void setId(int id) {
        if (this.id == 0){
            this.id = id;
        }
    }
    public String getName(){ return this.name; }
    public Type getType(){ return this.type; }

    public int getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return  "\n" +
                "Организация" + "\n" +
                "Код:" + id + "\n" +
                "Название:" + name + "\n" +
                "Вид:" + type.getFullName() +
                "\n";
    }

    /**
     * Unique magic calculation to get how much money organization get for the next period of time
     * @return how much money the company get or lost
     */
    public abstract double getNextFinancialReport();
}
