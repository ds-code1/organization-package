package Organization;

import TUI.PrintUtils;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
@SuppressWarnings("SpellCheckingInspection")
public class OrganizationRegistry {
    private final Set<Organization> registry = new HashSet<>();

    public OrganizationRegistry(PrintUtils printUtils){
        this.printUtils = printUtils;
    }
    private final PrintUtils printUtils;

    public Set<Organization> getRegistry(){ return registry; }

    public void printList() {
        if(registry.isEmpty()) System.out.println("Реестр пуст");
        else{
            printList(registry);
        }
    }

    public void printList(Collection<Organization> organizationList) {
        List<List<String>> table = new ArrayList<>();
        organizationList.forEach(organization -> {
            table.add(List.of(Integer.toString(organization.getId()), organization.getName(), organization.getType().getShortName()));
        });
        printUtils.printTableWithHeader(table, List.of("ID", "Название", "Тип"), List.of(13, 40, 8));
    }

    public List<Organization> findMatchingByPartOfName(String searchRequest){
        return registry.stream().filter(org -> org.getName().toLowerCase().contains(searchRequest)).toList();
    }

    public Organization getById(int id){
        return registry.stream().filter(org -> org.getId() == id).findAny().orElse(null);
    }

    private int generateUID() {
        int randomNum;
        do {
            randomNum = ThreadLocalRandom.current().nextInt(10000000, 99999999 + 1);
        } while (getById(randomNum) != null);
        return randomNum;
    }

    public boolean add(Organization organization){
        organization.setId(generateUID());
        return registry.add(organization);
    }

    public boolean delete(int id) {
        return registry.remove(getById(id));
    }
    public boolean delete(Organization organizationToDelete) {
        return registry.remove(organizationToDelete);
    }
}
