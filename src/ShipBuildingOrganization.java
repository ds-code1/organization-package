import Organization.*;

public class ShipBuildingOrganization extends Organization {
    public ShipBuildingOrganization(String name, Type type) {
        super(name, type);
    }

    @Override
    public double getNextFinancialReport() {
        int amountOfPeopleLiving = 4569;
        double averageTax = 4652.85;
        double averageRepairsCost = 438630.45;
        double averageInvestmentsAmount = 546899.74;
        return (Math.random() + 0.8) * averageTax * amountOfPeopleLiving + (Math.random() + 0.3) * averageInvestmentsAmount + averageRepairsCost;
    }
}
