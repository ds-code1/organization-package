package TUI;

import java.util.*;
@SuppressWarnings("SpellCheckingInspection")
public class Menu {

    public Menu(PrintUtils printUtils, List<Option> options){
        this.printUtils = printUtils;
        this.menuOptions = options;
    }

    private final PrintUtils printUtils;

    private static final Scanner scanner = new Scanner(System.in);

    public record Option(String name, Runnable action){}

    private final List<Option> menuOptions;

    public void printMainMenu(){
        List<String> menuColumn = new ArrayList<>();
        for(int i = 0; i < menuOptions.size(); i++){
            menuColumn.add(i + ".  " + menuOptions.get(i).name);
        }
        printUtils.printColumn(menuColumn, 40);
    }

    public static String getUserInput(String description){
        String userInput;
        do{
            System.out.print(description + ": ");
            scanner.skip("\n");
            userInput = scanner.nextLine().strip();
        } while (userInput.isBlank());
        return userInput;
    }

    public static <T> T getUserSelection (String description, List<T> options){
        int userInput;
        do{
            System.out.print(description + ": ");
            try{
                userInput = scanner.nextInt();
            }
            catch(NumberFormatException e){
                System.out.print("Введены некорректные данные");
                userInput = -1;
            }
        } while (userInput >= options.size() || userInput < 0);
        return options.get(userInput);
    }
    @SuppressWarnings("InfiniteLoopStatement")
    public void run(){
        while (true){
            printMainMenu();
            Option selectedOption = getUserSelection("Выберите", menuOptions);
            if(selectedOption != null)
                    selectedOption.action.run();
        }
    }
}
