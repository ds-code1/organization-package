package TUI;

import javax.swing.border.Border;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Simple printable ASCII table realisation
 * Row is a minimum table value to operate with
 */
public class PrintUtils {
    private final String topLeftCorner = "┌";
    private final String topRightCorner = "┐";
    private final String bottomLeftCorner = "└";
    private final String bottomRightCorner = "┘";
    private final String horizontalLine = "─";
    private final String verticalLine = "│";
    private final String leftRowConnection = "├";
    private final String rightRowConnection = "┤";
    private final String middleColumnConnection = "┼";
    private final String topColumnConnection = "┬";
    private final String bottomColumnConnection = "┴";
    private final String limitationIndicator = "...";

    public void printTable(List<List<String>> content, List<Integer> widths, List<Border> borders) {
        if(content.isEmpty()) return;
        if (content.get(0).size() != widths.size()) throw new RuntimeException("Please, provide correct max lengths for every column");

        if(borders.contains(Border.TOP)) printHorizontalBorder(HorizontalBorderDirection.DOWN, widths);

        printRow(content.get(0), widths);
        for (int i = 2; i < content.size();i++) {
            printHorizontalBorder(HorizontalBorderDirection.BOTH, widths);
            printRow(content.get(i), widths);
        }


        if(borders.contains(Border.BOTTOM)) printHorizontalBorder(HorizontalBorderDirection.UP, widths);
    }

    public void printTableWithHeader(List<List<String>> content, List<String> header, List<Integer> widths) {
        if(content.isEmpty()) return;
        if (header.size() != widths.size()) throw new RuntimeException("Please, provide correct max lengths for every column");

        printHeader(header, widths);
        printTable(content, widths, List.of(Border.BOTTOM));
    }

    public void printColumn(List<String> content, int width, List<Border> borders){
        if(content.isEmpty()) return;
        if(borders.contains(Border.TOP)) printHorizontalBorder(HorizontalBorderDirection.DOWN, List.of(width));

        for (String rowContent :
                content) {
            printRow(List.of(rowContent), List.of(width));
        }

        if(borders.contains(Border.BOTTOM)) printHorizontalBorder(HorizontalBorderDirection.UP, List.of(width));
    }

    public void printColumn(List<String> content, int width){
        printColumn(content, width, List.of(Border.BOTTOM, Border.TOP));
    }

    public void printColumn(List<String> content, int width, String header) {
        if(content.isEmpty()) return;
        printHeader(List.of(header), List.of(width));
        printColumn(content, width, List.of(Border.BOTTOM));
    }

    private List<String> prepareStrings(List<String> strings, int width) {
        return prepareStrings(strings, Collections.nCopies(strings.size(), width));
    }

    private List<String> prepareStrings(List<String> strings, List<Integer> widths) {
        ArrayList<String> output = new ArrayList<>(strings.size());
        output.addAll(strings);
        for (int i = 0; i < strings.size();i++) {
            if (output.get(i).length() > (widths.get(i) - limitationIndicator.length())) {
                output.set(i, output.get(i).substring(0, widths.get(i) - limitationIndicator.length()) + limitationIndicator);
            }
            else{
                output.set(i, output.get(i) + " ".repeat(widths.get(i) - output.get(i).length()));
            }
        }
        return output;
    }

    public void printHeader(List<String> headerContent, List<Integer> widths){
        if(headerContent.isEmpty()) return;
        printRow(headerContent, widths, List.of(Border.TOP));
        printHorizontalBorder(HorizontalBorderDirection.BOTH, widths);
    }

    public void printHorizontalBorder(HorizontalBorderDirection direction, List<Integer> widths){
        String leftSign = "";
        String middleSign = "";
        String rightSign = "";
        switch (direction){
            case BOTH -> {
                leftSign = leftRowConnection;
                middleSign = middleColumnConnection;
                rightSign = rightRowConnection;
            }
            case UP -> {
                leftSign = bottomLeftCorner;
                middleSign = bottomColumnConnection;
                rightSign = bottomRightCorner;
            }
            case DOWN -> {
                leftSign = topLeftCorner;
                middleSign = topColumnConnection;
                rightSign = topRightCorner;
            }
        }

        StringBuilder horizontalBorder = new StringBuilder(leftSign + " ")
                .append(horizontalLine.repeat(widths.get(0) + 1));

        for (int i = 1; i < widths.size();i++) {
            horizontalBorder.append(middleSign).append(" ")
                    .append(horizontalLine.repeat(widths.get(i) + 1));
        }
        horizontalBorder
                .append(rightSign);
        System.out.println(horizontalBorder);
    }

    public void printRow(List<String> content, List<Integer> widths){
        printRow(content, widths, List.of());
    }

    public void printRow(List<String> content, List<Integer> widths, List<Border> borders){
        if(content.isEmpty()) return;
        if(borders.contains(Border.TOP)) printHorizontalBorder(HorizontalBorderDirection.DOWN, widths);

        StringBuilder row = new StringBuilder(verticalLine);

        for (String cellContent:
             prepareStrings(content, widths)) {
            row.append(" ").append(cellContent)
                    .append(" " + verticalLine);
        }

        System.out.println(row);

        if(borders.contains(Border.BOTTOM)) printHorizontalBorder(HorizontalBorderDirection.UP, widths);
    }

    public enum HorizontalBorderDirection{
        UP, DOWN, NONE, BOTH
    }

    public enum Border{
        TOP, BOTTOM
    }
}
